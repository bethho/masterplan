# My Plan
###### tags: `plan`

### Goal
1. 增強程式能力（需要達到自行找尋解法、拿到程式要有辦法debug和修改、寫程式需要clean）
2. 托福須達到80
3. 需會看Paper

### 執行方式
1.	增強程式能力
利用課程程式來練習，目前會接觸的程式：
    * Python
    * C++
    * Verilog
2.	托福須達到80（每天練習1hr）
[TOEFL iBT® Test](https://www.ets.org/toefl/test-takers/ibt/register/how-to-register.html)

    |time|context|
    |----|------|
    |9/25-9/29|選擇一篇3min內的英文聽力，隔天跟二中練習。練習內容(1)互相念給對方(2)選擇文章的人要分享有哪些單字的衍生
    |9/30|找托福模擬試題練習|
    |10/1-11/30|先練習基本功（先練習聽力和口說）|
    |12/1|找托福模擬試題練習並查詢2023考試時間|
    |12/1-2/28|針對托福考題加強練習並規劃三月後的練習計畫
    |3/1-4/30||
    |5/1-6/30||
    |7/1-7/30||


3.	需會看Paper
    利用上課和實驗室須看paper練習，每一段都要寫評論
    目前需閱讀：
    * [Low-latency, high-throughput garbage collection](https://dl.acm.org/doi/10.1145/3519939.3523440)
    * [Garbage-first Garbage Collection](https://doi.org/10.1145/1029873.1029879)
    * [ Shenandoah: An open-source concurrent
compacting garbage collector for OpenJDK](https://doi.org/10.1145/2972206.2972210)

